import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, View, Text, Alert } from 'react-native';
import { Container, Button } from 'native-base';
import * as firebase from 'firebase';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from './Screens/Main';
import firebaseConfig from './firebase/db';

import Landing from './Screens/Landing';
import { Register } from './Screens/Register';
import { Login } from './Screens/Login';

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}

const Stack = createStackNavigator();

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      loggedIn: false,
    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.setState({
          loaded: false,
          loggedIn: false,
        });
      } else {
        this.setState({
          loaded: true,
          loggedIn: true,
        });
      }
    });
  }

  render() {
    const { loaded, loggedIn } = this.state;
    if (!loggedIn) {
      return (
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Landing">
            <Stack.Screen
              name="Landing"
              component={Landing}
              options={{ headerShown: false }}
            />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Login" component={Login} />
          </Stack.Navigator>
          <StatusBar barStyle="light-content" />
        </NavigationContainer>
      );
    } else {
      return <Main />;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 50,
  },
  registerButton: {
    flex: 1,
    padding: 20,
    backgroundColor: '#1a7212',
    marginHorizontal: 15,
    marginTop: 50,
  },
  btntext: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-light',
    fontSize: 18,
  },
});
