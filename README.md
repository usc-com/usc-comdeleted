# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* Quick summary
This is the repository for the Software Engineering Group Project. 

# How do I get set up? #

### Summary of set up ###
* For this project, you will need to have [Visual Studio Code](https://code.visualstudio.com/), Expo, [Node](https://nodejs.org/en/)(LTS Version), and [Sourcetree](https://www.sourcetreeapp.com/) installed on your device. Most dependencies and configuration files will be preinstalled before being available to this repository but there are some things that you will need to install to get your development environment set up. Git will also come in handy if you'd like to work in the terminal to pull or push changes to this repository.
### Visual Studio Code and Extensions ###
* First, you'll need to have [Visual Studio Code](https://code.visualstudio.com/) installed on your device. On Visual Studio Code, install the extensions ESLint and Prettier for better error detection and code formatting. In VS Code settings, enable Prettier as the default formatter by going into the settings and typing in format. "Editor: Default Formatter" should show up. From the dropdown menu, choose 'esbenp.prettier-vscode'. Also make sure that you check "Editor: Format On Save".
### Expo ###
* For [Expo](expo.io), you will need to register an account. To install expo on your computer, run the following command on your terminal in any directory.
```
npm install --global expo-cli
```
* Verify that expo is installed by running
```
expo whoami
```
* Login with your expo account by running
```
expo login
```
* To be able to  test the application with your mobile device, you will need to install the Expo app on your device and login with the same account.

### Sourcetree ###
Bitbucket will be used to host the repository and to make it easier to collaborate throughout the project. You can also install [Sourcetree](https://www.sourcetreeapp.com/) to interact with the project.

# How to get the project? #

### Cloning the repository ###
* You will first need to have [Sourcetree](https://www.sourcetreeapp.com/) downloaded, and logged into the same account you used to access this bitbucket repository.
* After adding your account on Sourcetree, you can now clone the repository to start developing.
* Click the clone option in your Sourcetree console and in the source path, enter "https://keilonrobertsonalpha@bitbucket.org/usc-com/usc-com.git". Wait for a second or two until the advanced options menu is available. Make sure the checkout branch is set to master and click clone.
* The project will now be available in the destination path you selected.
* Finally, run the following command to install all the node dependencies for this project
```
npm install
```


### Creating your own branch ###
* In order to contribute to the project, you must create your own local and remote branch, so that they can be merged into the master.
* To create your own branch, in your repository on Sourcetree, select Branch in your console, which will open up a window with details.
* Make sure your current branch says 'master', then enter your firstname and last initial in the New Branch textbox
```
Jack Harper - jackh
```
* Make sure working copy parent is selected, Checkout New Branch is checked and click Create Branch.
* Next, Click Push in your console
* Make sure the only local branch that is checked is the name of your branch, and that track is also checked.
* You have now created your own local and remote branches of the project and are ready to start development.

# Contribution guidelines #

### Each Session ###
* At the start of each development session, make sure to always fetch the latest changes from the master branch.
* You can do this in Sourcetree by clicking fetch, then clicking on Pull.
* In the Pull menu, make sure that the "Remote branch to pull" is set to master, and click pull
* This will keep your branch up to date with the master branch
* After doing this, be sure to commit those changes and push to your remote branch
* Sometimes however, merge conflicts can occur.

### Merge Conflicts ###
* As much as possible, try not to work on the same file at the same time to avoid merge conflicts when pulling changes. However, sometimes, you may need to change a few things.
* Sometimes, when trying to merge the master branch into your own branch, you may encounter merge conflicts.
* You can right click on the conflicted files in Sourcetree and either accept the master branch's version, or keep your version.
* You can also go to the files that have the conflicts, and try to sort them out from there.

### Committing Changes ###
* Whenever changes are made to your local branch, you should commit those changes, and push them to your remote branch.
* You can push changes to your remote branch immediately after committing by checking that option under your description.
* Please provide a descriptive title, and provide insight on the changes you have made.

### Pull Requests ###
* Pro-tip - Run the following command in your command line before creating a pull request
```
npm run format
```
* When you are done with a particular feature, you can create a pull request so that the team leads can review and merge the changes to the master branch.
* In Sourcetree, you can do this by first fetching the latest state of the project and then pulling the changes.
* Next, right click on your branch under "BRANCHES", and click Create Pull Request.
* This will take you to the Bitbucket site to create your Pull Request.
* Please provide a descriptive Title and a thorough description of your changes.
* Make sure that the source is your personal branch, the destination is master, and hit create pull request.

# Testing #

### Expo Testing ###
* To run the application on your mobile phone for tesing, make sure that you are logged in on your phone, favoirte browser and command line. Once you are logged in, run the following command in the project folder
```
expo start
```
* This will build the application and will give you a QR Code so that your phone will be able to connect to the application. It will also open up an interface in your browser with the same interface. 
* Scan the QR Code on the expo app on your android phone. 
* For iOS, use the QR scanner with the camera app. 
* If you run into issues, go to the web interface in your browser, and choose 'Tunnel' instead of 'LAN'. This will rebuild the application to have a direct connection to your phone. Your phone and your computer must be on the same Network. On your mobile phone, there will be a link to the application under 'Recently Developed'. If you have any other issues, check for the specific error online, or feel free to contact me.

# Documentation Links #

* [React Native Documentation](https://reactnative.dev/docs/getting-started)
* [Nativebase Documentation](https://docs.nativebase.io/)
* [Firebase Documentation](https://firebase.google.com/docs)
* [Expo Documentation](https://docs.expo.io/)
* [JavaScript Documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript)


# Who do I talk to? #

* Repo owner or admin
* Your team's techleads