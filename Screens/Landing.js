import React, { Component } from 'react';
import { Text, View, StyleSheet, SafeAreaView, Image } from 'react-native';
import {
  Container,
  Header,
  Content,
  Button,
  Left,
  Title,
  Right,
  Body,
  Icon,
} from 'native-base';
import { StatusBar } from 'expo-status-bar';

export default function Landing({ navigation }) {
  return (
    <Container>
      <View style={styles.container}>
        <Image style={styles.img} source={require('../assets/connect.jpg')} />
        <Text style={styles.h1}>Hello!</Text>
        <View>
          <Text style={styles.paragraph}>
            Stay up-to-date and connected with USC's latest events and news.
          </Text>
        </View>
        <View style={styles.btnSection}>
          <Button
            rounded
            style={styles.loginButton}
            onPress={() => navigation.navigate('Login')}
          >
            <Text style={(styles.text, styles.loginText)}>LOGIN</Text>
          </Button>
          <View style={styles.btnSection}>
            <Button
              rounded
              style={styles.registerButton}
              onPress={() => navigation.navigate('Register')}
            >
              <Text style={(styles.text, styles.registerText)}>SIGN UP</Text>
            </Button>
          </View>
        </View>
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  h1: {
    fontWeight: 'bold',
    fontSize: 50,
    fontFamily: 'sans-serif-light',
  },
  container: {
    fontFamily: 'sans-serif-light',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    marginTop: StatusBar.currentHeight,
    textAlign: 'center',
  },
  loginButton: {
    flex: 1,
    padding: 20,
    backgroundColor: '#1a7212',
    marginHorizontal: 15,
  },
  registerButton: {
    flex: 1,
    padding: 20,
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#1a7212',
    marginHorizontal: 15,
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-light',
    fontSize: 18,
  },
  registerText: {
    color: '#1a7212',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-light',
    fontSize: 18,
  },
  paragraph: {
    fontFamily: 'sans-serif-light',
    padding: 50,
    fontSize: 18,
    textAlign: 'center',
  },
  text: {
    fontFamily: 'sans-serif-light',
    fontSize: 18,
    marginHorizontal: 20,
  },
  img: {
    width: 250,
    height: 200,
    resizeMode: 'stretch',
    marginVertical: 30,
  },
  btnSection: {
    marginTop: 15,
  },
});
