import React, { Component } from 'react';
import { StyleSheet, View, Image, Alert } from 'react-native';
import {
  Container,
  Header,
  Content,
  Button,
  Form,
  Item,
  Input,
  Text,
  Icon,
  Label,
  Toast,
  Picker,
} from 'native-base';
import * as firebase from 'firebase';

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };

    this.onLogin = this.onLogin.bind(this); // bind this to the function
  }

  onLogin() {
    const { email, password } = this.state; // destructure the state's variables
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password) // firebase authentication function
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        var errMsg = error.toString();
        Alert.alert('Warning', errMsg, [{ text: 'OK' }]);
      });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <View style={styles.image}>
            <Image style={styles.img} source={require('../assets/user.png')} />
          </View>
          <Form>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Email Address</Label>
              <Input onChangeText={(email) => this.setState({ email })} />
            </Item>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={(password) => this.setState({ password })}
              />
            </Item>
            <Button
              rounded
              style={styles.registerButton}
              block
              onPress={() => this.onLogin()}
            >
              <Text style={styles.registerText}>LOG IN</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
  },
  h1: {
    fontWeight: 'bold',
    fontSize: 30,
    fontFamily: 'sans-serif-light',
    marginTop: 20,
    marginHorizontal: 20,
    textAlign: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  registerButton: {
    flex: 1,
    padding: 20,
    backgroundColor: '#1a7212',
    marginHorizontal: 15,
    marginTop: 50,
  },
  registerText: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-light',
    fontSize: 18,
  },
  picker: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  text: {
    fontFamily: 'sans-serif-light',
    fontSize: 18,
  },
  img: {
    width: 100,
    height: 100,
    resizeMode: 'stretch',
    marginVertical: 10,
  },
});
