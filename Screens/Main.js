import React, { Component } from 'react';
import News from './News.js';
import Notices from './Notices.js';
import Calendar from './Calendar.js';
import Resources from './Resources.js';
import { Button, Text, Icon, Footer, FooterTab } from 'native-base';
import * as firebase from 'firebase';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationContainer } from '@react-navigation/native';

const Tab = createBottomTabNavigator();

export default function Main() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="News"
        barStyle={{ backgroundColor: '#1a7212' }}
        tabBarOptions={{
          activeTintColor: '#1a7212',
        }}
      >
        <Tab.Screen
          name="News"
          component={News}
          options={{
            tabBarLabel: 'News',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="newspaper"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Notices"
          component={Notices}
          options={{
            tabBarLabel: 'Notices',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="bell" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="Calendar"
          component={Calendar}
          options={{
            tabBarLabel: 'Calendar',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="calendar"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Resources"
          component={Resources}
          options={{
            tabBarLabel: 'Resources',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="file" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
