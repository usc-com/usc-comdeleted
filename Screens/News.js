import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { Button } from 'native-base';
import * as firebase from 'firebase';


export default class News extends Component {
  onSignOut() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        Alert.alert('Success', 'You have been logged out', [{ text: 'OK' }]);
      })
      .catch((error) => {
        var errMsg = error.toString();
        Alert.alert('Warning', errMsg, [{ text: 'OK' }]);
      });
  }
  render() {
    return (
      <View styles={styles.container}>
        <Text> News Tab </Text>
        <Button onPress={this.onSignOut}>
        <Text>SIGN OUT</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
