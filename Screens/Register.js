import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, View, Image, Alert, ScrollView } from 'react-native';
import {
  Container,
  Header,
  Content,
  Button,
  Form,
  Item,
  Input,
  Text,
  Icon,
  Label,
  Picker,
} from 'native-base';
import * as firebase from 'firebase';

export class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      verifiedpassword: '',
      stuID: '',
      firstname: '',
      lastname: '',
      campus: 'Main',
      school: 'School of Science and Technology',
    };

    this.onSignUp = this.onSignUp.bind(this); //bind this to the function
  }

  onSignUp() {
    const {
      email,
      password,
      verifiedpassword,
      stuID,
      firstname,
      lastname,
      campus,
      school,
    } = this.state; // destructure the state's variables into separate variables
    if (firstname == '' || lastname == '') {
      Alert.alert('Warning', 'Please enter your full name', [{ text: 'OK' }]);
    } else if (password != verifiedpassword) {
      Alert.alert('Warning', 'Passwords do not match!!!', [{ text: 'OK' }]);
    } else if (!email.includes('usc.edu.tt')) {
      Alert.alert('Warning', 'Email is not of the university domain!!', [
        { text: 'OK' },
      ]);
    } else {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password) // firebase authentication function
        .then((result) => {
          firebase
            .firestore()
            .collection('Student')
            .doc(firebase.auth().currentUser.uid)
            .set({
              firstname,
              lastname,
              email,
              stuID,
              campus,
              school,
            });
          Alert.alert(
            'Success',
            'Your account has been created, and you have been logged in!!',
            [{ text: 'OK' }],
          );
        })
        .catch((error) => {
          var errMsg = error.toString();
          Alert.alert('Warning', errMsg, [{ text: 'OK' }]);
        });
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Content>
          <View>
            <Text style={styles.text}> Create Account </Text>
          </View>
          <Form>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Student ID</Label>
              <Input onChangeText={(stuID) => this.setState({ stuID })} />
            </Item>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>First Name</Label>
              <Input
                onChangeText={(firstname) => this.setState({ firstname })}
              />
            </Item>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Last Name</Label>
              <Input onChangeText={(lastname) => this.setState({ lastname })} />
            </Item>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Email Address</Label>
              <Input onChangeText={(email) => this.setState({ email })} />
            </Item>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={(password) => this.setState({ password })}
              />
            </Item>
            <Item style={styles.items} floatingLabel>
              <Label style={styles.text}>Verify Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={(verifiedpassword) =>
                  this.setState({ verifiedpassword })
                }
              />
            </Item>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="Select Campus"
                placeholderStyle={{ color: '#bfc6ea' }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.campus}
                onValueChange={(campus) => this.setState({ campus })}
              >
                <Picker.Item label="Main" value="Main" />
                <Picker.Item label="Tobago" value="Tobago" />
                <Picker.Item label="South Trinidad" value="South Trinidad" />
                <Picker.Item label="Guyana" value="Guyana" />
                <Picker.Item label="St. Lucia" value="St. Lucia" />
                <Picker.Item label="Barbados" value="Barbados" />
              </Picker>
            </Item>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="Select School"
                placeholderStyle={{ color: '#bfc6ea' }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.school}
                onValueChange={(school) => this.setState({ school })}
              >
                <Picker.Item
                  label="School of Science and Technology"
                  value="School of Science and Technology"
                />
                <Picker.Item
                  label="School of Business and Entrepreneurship"
                  value="School of Business and Entrepreneurship"
                />
                <Picker.Item
                  label="School of Education and Humanities"
                  value="School of Education and Humanities"
                />
                <Picker.Item
                  label="School of Theology and Religion"
                  value="School of Theology and Religion"
                />
                <Picker.Item
                  label="School of Social Sciences"
                  value="School of Social Sciences"
                />
              </Picker>
            </Item>
            <View>
              <Button
                rounded
                style={styles.registerButton}
                block
                onPress={() => this.onSignUp()}
              >
                <Text style={styles.registerText}>SIGN UP</Text>
              </Button>
            </View>
          </Form>
        </Content>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
  },
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
  h1: {
    fontWeight: 'bold',
    fontSize: 30,
    fontFamily: 'sans-serif-light',
    marginTop: 20,
    marginHorizontal: 20,
    textAlign: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  registerButton: {
    flex: 1,
    padding: 20,
    backgroundColor: '#1a7212',
    marginHorizontal: 15,
    marginTop: 50,
  },
  registerText: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-light',
    fontSize: 18,
  },
  picker: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  text: {
    fontFamily: 'sans-serif-light',
    fontSize: 15,
  },
  img: {
    width: 100,
    height: 100,
    resizeMode: 'stretch',
    marginVertical: 10,
  },
});

/**
 *
 */
